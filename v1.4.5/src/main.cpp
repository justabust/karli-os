#include <KiWi/KW_gui.h>
#include <KiWi/KW_frame.h>
#include <KiWi/KW_label.h>
#include <KiWi/KW_editbox.h>
#include <KiWi/KW_scrollbox.h>
#include <KiWi/KW_button.h>
#include <KiWi/KW_renderdriver_sdl2.h>
#include <SDL2/SDL.h>
#include "IP.hpp"
#include "Socket.hpp"

bool quit = KW_FALSE;
void quitButton(KW_Widget * widget, int b) {
  quit = KW_TRUE;
  printf("BYEE");
}

int main(int argc, char ** argv) {
  SDL_Event ev;
  SDL_Window * window;
  SDL_Renderer * renderer;
  KW_RenderDriver * driver;
  KW_RenderDriver * driverTwo;
  KW_Surface * set;
  KW_GUI * gui;
  KW_Font * font, *fontTwo;
  KW_Rect geometry = {0, 0, 320, 240};
  KW_Rect titlerect = { titlerect.x = geometry.w * 2 - 250, titlerect.y = 10, titlerect.w = 300, titlerect.h = 30 };
  KW_Widget * frame, * button, * title, *buttonOne;
  int i;
  
  /* initialize SDL */
  SDL_Init(SDL_INIT_EVERYTHING);
  SDL_CreateWindowAndRenderer(geometry.w, geometry.h, SDL_WINDOW_FULLSCREEN_DESKTOP, &window, &renderer);
  SDL_SetRenderDrawColor(renderer, 200, 1, 1, 1);
  driver = KW_CreateSDL2RenderDriver(renderer, window);
  set = KW_LoadSurface(driver, "./tiles/tileset.png");
  
  /* FONT */
  gui = KW_Init(driver, set);
  font = KW_LoadFont(driver, "./font/Exo-Light.ttf", 30);
  KW_SetFont(gui, font);

  geometry.x = (unsigned)(geometry.w * 0.0625f);
  geometry.y = (unsigned)(geometry.h * .0625f);
  geometry.w *= .875f;
  geometry.h *= .875;
  frame = KW_CreateFrame(gui, NULL, &geometry);

  title = KW_CreateLabel(gui, frame, "KARLAE OS v1.2.4", &titlerect);
  if (ev.type == SDL_WINDOWEVENT && ev.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
        geometry.w = (unsigned)ev.window.data1;
        geometry.h = (unsigned)ev.window.data2;
        geometry.x = (unsigned)(geometry.w * 0.0625);
        geometry.y = (unsigned)(geometry.h * .0625);
        geometry.w *= .875f;
        geometry.h *= .875;
        KW_SetWidgetGeometry(frame, &geometry);
        KW_SetWidgetGeometry(title, &titlerect);
        //KW_SetWidgetGeometry(buttonOne, &buttonrect);
  }
  //BUTTON QUIT
  KW_Rect buttonrect = { .x = geometry.w * 4 - 100  , .y = geometry.y, .w = 40, .h = 40 };
  buttonOne = KW_CreateButtonAndLabel(gui, frame, "X", &buttonrect);
  KW_AddWidgetMouseDownHandler(buttonOne, quitButton);

  geometry.x = 10; geometry.y = 0; geometry.h = 40; geometry.w = 230;
  /* create another parent frame */
  while (!SDL_QuitRequested() && !quit) {
    while (SDL_PollEvent(&ev)) {
      if (ev.type == SDL_WINDOWEVENT && ev.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
        geometry.w = (unsigned)ev.window.data1;
        geometry.h = (unsigned)ev.window.data2;
        geometry.x = (unsigned)(geometry.w * 0.0625);
        geometry.y = (unsigned)(geometry.h * .0625);
        geometry.w *= .875f;
        geometry.h *= .875;
        KW_SetWidgetGeometry(frame, &geometry);
        KW_SetWidgetGeometry(title, &titlerect);
        KW_SetWidgetGeometry(buttonOne, &buttonrect);
      }
    }
    SDL_RenderClear(renderer);
    KW_ProcessEvents(gui);
    KW_Paint(gui);
    SDL_RenderPresent(renderer);
    SDL_Delay(1);
  }
  KW_ReleaseFont(driver, font);
  KW_Quit(gui);
  KW_ReleaseSurface(driver, set);
  SDL_Quit();
  
  return 0;
}